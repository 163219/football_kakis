var express = require ("express");

var app = express();

var handlebars = require("express-handlebars");

app.engine("handlebars",
    handlebars({
        defaultLayout: "index",
        helpers: {
            isActive: function(view, actual){
                if (view == actual)
                    return ("active");
                return ("");
            }
        }
    }));

app.set("view engine", "handlebars");

var config = {
    connectionLimit: 5,
    host: "173.194.229.7",
    user: "ttc",
    password: "ttc",
    database: "ttc_db"
};

var mysql =require("mysql");
var connPool = mysql.createPool(config);

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/",function(req, res) {
    res.render("index");
});

app.get("/menu/:category", function (req, res) {
    console.info(">>> category %s", req.params.category);
    var cat = req.params.category;
    switch (cat) {
        case "index":
            res.render(cat);
            break;
        case "organise":
            res.render(cat);
            break;
        case "book":
            res.render(cat);
            break;
        case "login":
            res.render(cat);
            break;
        case "profile":
            res.render(cat);
            break;
        default:
            res.redirect("/");
    }
});

app.use(express.static(__dirname + "/public"));

app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.use(function(req, res, next){
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/");
});

app.set('port', process.env.APP_PORT || 3000);

app.listen(app.get('port'), function(){
    console.log('Express started on http://localhost:' + app.get ('port')+ '; press Ctrl-C to end')
});

